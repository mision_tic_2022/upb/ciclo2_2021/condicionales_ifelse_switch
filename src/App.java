import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        int numero_1 = 5;
        int numero_2 = 8;
        int numero_3 = 10;
        /**
         * ---------IF-ELSE--------
         */
        /*
        //Condicionales
        //numero1 es mayor a numero 2 Y numero1 es menor a numero3
        //&& -> and -> y
        //|| -> or -> o
        if( (numero_1 > numero_2) && (numero_1 < numero_3)){
            //if anidado
            if(numero_1 > numero_2){

            }else{

            }

        //if en cascada
        }else if(numero_1 < numero_2 || numero_1 > numero_3){

        }else{

        }
    }
    */



    /**********************
     * -----SWITCH-CASE
     **********************/
    switch (numero_1){
        case 1:
            System.out.println("es de valor 1");
            break;
        case 5:
            System.out.println("es de valor 5");
            break;
        
        default:
            System.out.println("se asignará otro valor");
            break;
    }

  
    

    }
}
