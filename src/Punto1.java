import java.util.Scanner;

public class Punto1 {
    public static void main(String[] args) throws Exception{
        //Pedir los datos al usuario
        try (Scanner entrada = new Scanner(System.in)){
            //Solicitar el primer número
            System.out.println("Por favor ingrese el primer número:");
            double numero_1 = entrada.nextDouble();
            //Solicitar el segundo número
            System.out.println("Por favor ingrese el segundo número:");
            double numero_2 = entrada.nextDouble();
            //Solicitar el tipo de operación que quiere realizar el usuario
            System.out.println("Por favor ingrese la operación a realizar (*, -, +, /)");
            String operacion = entrada.next();
            //Variable que almacena el resultado
            double resultado;
            //Evaluar las operaciones posibles ingresadas por el usuario
            switch (operacion){
                case "+":
                    resultado = numero_1 + numero_2;
                    break;
                case "-":
                    resultado = numero_1 - numero_2;
                    break;
                case "*":
                    resultado = numero_1 * numero_2;
                    break;
                case "/":
                    resultado = numero_1 / numero_2;
                    break;
                default:
                    resultado = 0.0;
                    break;
            }
            //Mostrar resultado
            System.out.println("El resultado es: "+resultado);

        } catch (Exception e) {
            System.out.println("Error");
            //TODO: handle exception
        }
    }
}
