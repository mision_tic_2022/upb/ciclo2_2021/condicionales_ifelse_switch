/**
 * Autor: Santiago Nicolas
 */
import java.util.Scanner;

public class Punto2 {

    public static void main(String[] args) {
        int meses;
        try(Scanner a = new Scanner(System.in)){
            System.out.println("Ingrese un numero del 1 al 6 ");
            meses = a.nextInt();

            switch (meses) {
                case 1:
                    System.out.println("El mes que selecciono es Enero ");
                    break;
                case 2:
                    System.out.println("El mes que selecciono es Febrero ");
                    break;
                case 3:
                    System.out.println("El mes que selecciono es Marzo ");
                    break;
                case 4:
                    System.out.println("El mes que selecciono es Abril ");
                    break;
                case 5:
                    System.out.println("El mes que selecciono es Mayo ");
                    break;
                case 6:
                    System.out.println("El mes que selecciono es Junio ");
                    break;
                default:
                    System.out.println("El numero ingresado no existe");
                    break;
            }
        }catch (Exception e) {
            System.out.println("Error");
        }
    }
    
}
