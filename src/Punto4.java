import java.util.Scanner;

public class Punto4 {
    public static void main(String[] args){

        try (Scanner entrada = new Scanner(System.in)){
            System.out.println("A qué departamento pertenece el empleado (TI, RH, AD, OP):");
            String departamento = entrada.next();
            int sueldo;
            switch (departamento){
                case "TI":
                    sueldo = 6000;
                    break;
                case "RH":
                    sueldo = 4500;
                    break;
                case "AD":
                    sueldo = 5500;
                    break;
                case "OP":
                    sueldo = 2500;
                    break;
                default:
                    sueldo = 0;
                    break;
            }
            System.out.println("Su sueldo es: "+sueldo);

        } catch (Exception e) {
            System.out.println("error");
            //TODO: handle exception
        }

    }
}
