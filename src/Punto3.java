/**
 * Autor: Edgar
 */

import java.util.Scanner;

public class Punto3 {
    public static void main(String[] args) throws Exception {
        
        int hijos;
        try (Scanner entrada = new Scanner(System.in)){
            
            System.out.print("Ingrese el número de hijos: ");
            hijos = entrada.nextInt();
            
            if(hijos < 0){
                System.out.println("No puede ingresar un valor negativo.");
            }
            else {
                switch (hijos) {
                    
                    case 0:
                        System.out.println("No recibirá subsidio.");
                        break;
                    
                    case 1:
                        System.out.println("Recibirá subsidio de $500.000.");
                        break;
                    
                    case 2:
                        System.out.println("Recibirá subsidio de $1200.000.");
                        break;
                    
                    case 3:
                        System.out.println("Recibirá subsidio de $1800.000");
                        break;
                    
                    default:
                        System.out.println("Recibirá un subsidio de $3200.000");
                        break;
                    }
            
            } 

    } catch (Exception e) {
        System.out.println("Error");
    }
    }   
}
